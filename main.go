package main

import (
	"fmt"
	"strings"
)

func Teclado ( legenda string) string {
	var entrada string =""
	for {
		fmt.Print(legenda, " Escolha , Pedra, papel ou Tesoura ")
		fmt.Scanln(&entrada)
		entrada = strings.ToLower(entrada)
		if entrada == "pedra" || entrada == "papel" || entrada == "tesoura" {
			fmt.Println("Jogador um escolheu", entrada, " ")
			return entrada
		} else {
			fmt.Println("Entrada invalida ")
		}
	}
}


func Play(p1 string, p2 string) string {
	retorno := ""
	if (p1 == "pedra" && p2 == "tesoura")  || ( p1 == "tesoura" && p2 == "papel"  ) || (p1 == "papel" && p2 == "pedra") {
		retorno = "Jogador 1 vence"
	}

	if (p2 == "pedra" && p1 == "tesoura")  || ( p2 == "tesoura" && p1 == "papel"  ) || (p2 == "papel" && p1 == "pedra") {
		retorno = "Jogador 2 vence"
	}
	return  retorno
}

func main(){

	jogador1 := Teclado("Jogador 1")
	jogador2 := Teclado("Jogador 2")

	fmt.Println( "Jogador 1 = ", jogador1 )
	fmt.Println( "Jogador 2 = ", jogador2 )

	if  jogador1 == jogador2 {
		fmt.Println( "Empate")
	}else {
		retorno := Play(jogador1, jogador2)
		fmt.Println(retorno)
	}

}